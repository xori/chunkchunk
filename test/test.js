'use strict';

let ChunkChunk = require('../index'),
    fs = require('fs'),
    crypto = require('crypto'),
    file = __dirname + '/test.jpg',
    stream = fs.openSync(file, 'r');

let t = process.hrtime(),
    chunk = new ChunkChunk(stream, {max:16 * 1024}),
    chunks = chunk.toEnd();
t = process.hrtime(t);
console.log(`${chunks.length} chunks in ${t[0]}s ${t[1]/1000000}ms`);

chunks.forEach(function (chunnk) {
  console.log(`${chunnk.buffer.length}\t${chunnk.hash}`);
});
console.log(chunk.fsize)

let fullSha = crypto.createHash('sha512'),
    fullBuffer = new Buffer(chunk.fsize);

t = process.hrtime();
fs.readSync(stream, fullBuffer, 0, fullBuffer.length, 0);
fullSha.update(fullBuffer)
let myhash = fullSha.digest('base64');
t = process.hrtime(t);
console.log(`file: ${myhash} in ${t[1]/1000000}ms`);


fs.close(stream);
