# Variable File Chunker

Uses the [BuzHash](http://git.verworn.net/buzhash) library to chop up files to
feature determined chunks. Good for deduplication.

    const ChunkChunk = require('chunkchunk'),
          fs = require('fs'),
          file = fs.openSync('my/file');

    const chunkee = new ChunkChunk(file, { max: 40 * 1024 })
    // create variable chunks of `my/file` to a maximum size
    // of 40k, with the default `min` chunk size of 50% of that.
    const chunk = chunkee.nextChunk();
    // { hash: 'sha256...', buffer: <Buffer ...> }
    const chunks = chunkee.toEnd();
    // [ {hash:.., buffer:<..>}, {...}]

    fs.close(file);

## ChunkChunk API

`new ChunkChunk(fd, options)`

- `fd`: an opened file descriptor, eg. `fd = fs.openSync('file', 'r')`
- `options`:
  - `max`: maximum chunk size. default 20kB.
  - `min`: minimum chunk size. default half of `max`. There is one exception to
    this parameter and that is the last chunk of a file, which can be smaller
    than this setting.
  - `features`: the number which to mod the rolling hash to, to find features in
    files. default is half of `min`

---

`ChunkChunk.prototype.hasChunks()`

Returns true if it hasn't reached end of file

---

`ChunkChunk.prototype.nextChunk()`

Returns the next variable size chunk.<br>
Returns:

- `buffer`: a nodejs `Buffer` object of that chunk
- `hash`: a sha256 hash of `buffer`

---

`ChunkChunk.prototype.toEnd()`

Returns _all_ variable size chunks left in the file, in an array.<br>
Returns:

- `Array<Chunk>`
  - `buffer`: a nodejs `Buffer` object of that chunk
  - `hash`: a sha256 hash of `buffer`

### Example Runs
The following is an example run of `npm test` chunking a ~120kb `.jpg` of
batman. First column is number of bytes in the chunk, with a max chunk setting
of 30k and a min of 15k. Second column is the sha256 of that chunk.

    [master] ~ npm test

    > chunkchunk@0.2.0 test C:\code\experiments\chunkchunk
    > node test/test

    6 chunks in 0s 26.978401ms
    30000   hOOcZnCdxAzoohOvQ4lc+GnrpHblYGS3d2zFNb61/ic=
    20442   HTHcDjW8XoAwc5MMDJ43kBNnQMf/8qPQ3ebfUbyxj+k=
    26863   ZXA2eQInJ0bTibfyiktzFx71CJHYP03DN+8kjr55Pcg=
    15458   dU0JcS9BfCGNDhb/iTt5FQSJby7oRXV2jAM4Uydsc14=
    15475   URAP2pD9bPjZwbvoBdvp3wiqmsTwZaNhKJfuGX/Yqok=
    10324   VJMCh8kNTYweKQng1XEEasdvsICgBc/hJWyqu+ha+Z8=
    118562

## TODO

- [ ] Make ChunkChunk take a string, instead of a file descriptor.
- [ ] This library screams to be made a Transform Stream.
- [X] Add config option for feature 'uniqueness'
