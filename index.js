'use strict';
/**
 *  ChunkChunk
 *
 *  var chunk = new ChunkChunk(fileStream, { max: 100, min: 50 })
 *  chunk.nextChunk(); // { hash: 'aaa...', buffer: < ... > }
 **/
const BuzHash = require('buzhash'),
      fs = require('fs'),
      crypto = require('crypto');

class ChunkChunk {
  constructor (file, options) {
    options     = options     || {};
    options.max = options.max || 20 * 1024 // 20kb max chunk size
    options.min = options.min || options.max / 2 // 10kb min chunk size
    options.features = options.features || options.min / 2
    this.file = file;
    this.fsize = fs.fstatSync(this.file).size;
    this.options = options;
    this.position = 0;
    this.buz = new BuzHash(this.options.min);
  }

  nextChunk () {
    // create a buffer of the largest chunk and start iterating.
    let hash, i,
        buffer = new Buffer(this.options.max);
    let size = fs.readSync(this.file, buffer, 0, this.options.max, this.position);

    // for each byte, increment the position & rolling hash.
    for(i = 0; i < size; i++) {
      let byte = buffer.readUInt8(i);
      this.position++;
      hash = this.buz.update(byte);

      // we're looking for a hash with the lowest 13 bits set to 0
      if (i > this.options.min && (hash % this.options.features) === 0
          || this.position == this.fsize) { // or at EOF
        // feature found! throw away the rest
        buffer = buffer.slice(0, i + 1);
        break;
      }
    }
    let sha = crypto.createHash('sha256')
    sha.update(buffer);
    return { hash: sha.digest('base64'), buffer: buffer };
  }

  hasNext () {
    return this.position < this.fsize;
  }

  toEnd () {
    let result = []
    while(this.hasNext()) {
      result.push(this.nextChunk());
    }
    return result;
  }
}
module.exports = ChunkChunk;
